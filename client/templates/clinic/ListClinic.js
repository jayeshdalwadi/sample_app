Template.ListClinic.helpers({

  /**
   * @returns {*} All of the Stuff documents.
   */
   clinicList: function () {
    return Clinic.find();
  }
});
