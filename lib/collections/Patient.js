patient  = "Patient";  // avoid typos, this string occurs many times.

Patient = new Mongo.Collection(patient);
Patient.before.insert(function (userId, doc)
{
  doc.created_at = new Date();
  doc.user_id = Meteor.userId() ;
});
Meteor.methods({
  /**
   * Invoked by AutoForm to add a new Stuff record.
   * @param doc The Stuff document.
   */
  addPatient: function(doc) {
    check(doc, Patient.simpleSchema());
    Patient.insert(doc);
  },
  /**
   *
   * Invoked by AutoForm to update a Stuff record.
   * @param doc The Stuff document.
   * @param docID It's ID.
   */
  editPatient: function(doc, docID) {
    check(doc, Patient.simpleSchema());
    Patient.update({_id: docID}, doc);
  }
});

// Publish the entire Collection.  Subscription performed in the router.
if (Meteor.isServer) {
  Meteor.publish(patient, function (user_id) {
    return Patient.find({user_id:user_id});
  });
  Meteor.publish("PatientShow", function (user_id,id) {
    return Patient.find({user_id:user_id,_id:id});
  });
}


/**
 * Create the schema for Stuff
 * See: https://github.com/aldeed/meteor-autoform#common-questions
 * See: https://github.com/aldeed/meteor-autoform#affieldinput
 */
 Patient.attachSchema(new SimpleSchema({
  fname: {
    label: "First Name",
    type: String,
    optional: false,
    autoform: {
      placeholder: "First Name"
    }
  },
  lname: {
    label: "Last Name",
    type: String,
    optional: false,
    autoform: {
      placeholder: "Last Name"
    }
  },
  age: {
    label: "Age",
    type: Number,
    optional: false,
    autoform: {
      placeholder: "3"
    }
  },
  question:{
    label: "What's bothering you?",
    type: String,
    optional: false,
    autoform: {
      placeholder: ""
    }
  },
  question1:{
    label: "Have you consulted any other doctor?",
    type: Boolean,
    optional: false,
    autoform: {
    }
  },
  question2:{
    label: "Who referred you to me?",
    type: String,
    optional: false,
    autoform: {
      placeholder: "Refrance Doctor Name"
    }
  },
  created_at : {
   type: Date,
   optional: false,
   autoform: {
     placeholder: "Refrance Doctor Name"
   }
  },
  user_id:{
   type: String,
   optional: false,
   autoform: {
     placeholder: "Refrance Doctor Name"
   }
  }
}));
