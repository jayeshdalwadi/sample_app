Template.Invite.helpers({

  /**
   * @returns {*} All of the Stuff documents.
   */
   clinicList: function () {
    return Clinic.find({user_id:Meteor.userId()});
  },
  inviteList: function () {
   return Invite.find({});
 }
});
Template.Invite.events({
  "click #btn_request": function(event, template){
    email = template.find('#email').value;
    refrance_id = template.find('#refrance_id').value;
    username = template.find('#username').value;
    dataObject = {email:email,refrance_id:refrance_id,username:username}
    Meteor.call("inviteUser", dataObject, function(error, result){
      if(error){
        console.log("error", error);
      }
      if(result){

      }
    });
  }
});
