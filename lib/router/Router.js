/**
 * Configure Iron Router.
 * See: http://iron-meteor.github.io/iron-router/
 */


Router.configure({
  layoutTemplate: 'Layout',
  waitOn: function() { return Meteor.subscribe("Stuff"); },
  loadingTemplate: 'Loading'
});

Router.route('/', {
  name: 'Home'
});

Router.route('/list', {
  name: 'ListStuff'
});

Router.route('/add', {
  name: 'AddStuff'
});
Router.route('/invite', {
  name: 'Invite',
  waitOn: function() { return Meteor.subscribe("Clinic",Meteor.userId()); }
});


Router.route('/stuff/:_id', {
  name: 'EditStuff',
  data: function() { return Stuff.findOne(this.params._id); }
});
Router.route('/patient/list', {
  name: 'ListPatient',
  waitOn: function() { return Meteor.subscribe("Patient",Meteor.userId()); }
});

Router.route('/patient/add', {
  name: 'AddPatient'
});

Router.route('/about', {
  name: 'About'
});
Router.route('/patient/:_id', {
  name: 'EditPatient',
  waitOn: function() { return Meteor.subscribe("PatientShow",Meteor.userId(),this.params._id); },
  data: function() { return Patient.findOne(this.params._id); }
});

Router.route('/clinic/list', {
  name: 'ListClinic',
  waitOn: function() { return Meteor.subscribe("Clinic",Meteor.userId()); }
});
Router.route('/clinic/add', {
  name: 'AddClinic'
});
Router.route('/clinic/:_id', {
  name: 'EditClinic',
  waitOn: function() { return Meteor.subscribe("ClinicShow",Meteor.userId(),this.params._id); },
  data: function() { return Clinic.findOne(this.params._id); }
});
