
invite  = "Invite";  // avoid typos, this string occurs many times.

Invite = new Mongo.Collection(invite);
Invite.before.insert(function (userId, doc)
{
  doc.created_at = new Date();
  doc.user_id = Meteor.userId();
});

Meteor.methods({
  /**
   * Invoked by AutoForm to add a new Stuff record.
   * @param doc The Stuff document.
   */
   inviteUser:function(doc){
      email = doc.email
      refrance_id = doc.refrance_id
      user = Meteor.users.find({'username': {'$regex': new RegExp(email, "i")}}).fetch()[0];
      console.log(user)
      // Meteor.users.findOne({emails: email})
      if(user){
        if (user._id == Meteor.userId()){
          return;
        }else{
          invite_list = typeof user.profile  == "undefined" ? [] : user.profile.invite_list
          if(typeof invite_list == "undefined"){
            invite_list =[]
          }
          if(invite_list.indexOf(refrance_id) < 0){
            invite_list.push(refrance_id)
            doc = {invited_user_id:Meteor.userId(),reference_id:refrance_id}
            console.log(doc)
            check(doc, Invite.simpleSchema());
            Invite.insert(doc);
          }
          Meteor.users.update({_id:user._id},{ $set: {"profile.invite_list":invite_list}});
        }
      }else{
        var invite_list = []
        invite_list.push(refrance_id)
        var profile = {invite_list:invite_list}
        user = Accounts.createUser({
            email: email,
            username: doc.username,
            password: "password",
            profile: profile
        });
        doc = {invited_user_id:Meteor.userId(),reference_id:refrance_id}
        check(doc, Invite.simpleSchema());
        Invite.insert(doc);
      }
   },
  addInvite: function(doc) {
    check(doc, Invite.simpleSchema());
    Invite.insert(doc);
  },
  /**
   *
   * Invoked by AutoForm to update a Stuff record.
   * @param doc The Stuff document.
   * @param docID It's ID.
   */
  editInvite: function(doc, docID) {
    check(doc, Invite.simpleSchema());
    Invite.update({_id: docID}, doc);
  }
});

// Publish the entire Collection.  Subscription performed in the router.
if (Meteor.isServer) {
  Meteor.publish(invite, function (user_id) {
    return Invite.find({user_id:user_id});
  });
  Meteor.publish("InviteShow", function (user_id,id) {
    return Invite.find({user_id:user_id,_id:id});
  });
}


/**      console.log(formType)
      console.log(result)
 * Create the schema for Stuff
 * See: https://github.com/aldeed/meteor-autoform#common-questions
 * See: https://github.com/aldeed/meteor-autoform#affieldinput
 */
 Invite.attachSchema(new SimpleSchema({
   reference_id: {
    label: "reference_id",
    type: String,
    optional: false,
    autoform: {
      placeholder: "Reference Id"
    }
  },
  invited_user_id: {
    label: "invited user",
    type: String,
    optional: false,
    autoform: {
      placeholder: "Invited user id"
    }
  },
  created_at : {
   type: Date,
   optional: true,
   autoform: {
     placeholder: ""
   }
  },
  user_id:{
   type: String,
   optional: true,
   autoform: {
     placeholder: ""
   }
  }
}));
