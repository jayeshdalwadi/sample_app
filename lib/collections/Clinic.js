
clinic  = "Clinic";  // avoid typos, this string occurs many times.

Clinic = new Mongo.Collection(clinic);
Clinic.before.insert(function (userId, doc)
{
  doc.created_at = new Date();
  doc.user_id = Meteor.userId() ;
});
Meteor.methods({
  /**
   * Invoked by AutoForm to add a new Stuff record.
   * @param doc The Stuff document.
   */
  addClinic: function(doc) {
    console.log("add call")
    check(doc, Clinic.simpleSchema());
    Clinic.insert(doc);
  },
  /**
   *
   * Invoked by AutoForm to update a Stuff record.
   * @param doc The Stuff document.
   * @param docID It's ID.
   */
  editClinic: function(doc, docID) {
    check(doc, Clinic.simpleSchema());
    Clinic.update({_id: docID}, doc);
  }
});

// Publish the entire Collection.  Subscription performed in the router.
if (Meteor.isServer) {
  Meteor.publish(clinic, function (user_id) {
    user = Meteor.users.find({_id:user_id}).fetch()[0]
    if (!user){
      return [];
    }
    invite_list = typeof user.profile  == "undefined" ? [] : user.profile.invite_list
    if(typeof invite_list == "undefined"){
      invite_list =[]
    }
    return Clinic.find({$or: [{user_id: user_id},{_id:{$in:invite_list}}]});
  });
  Meteor.publish("ClinicShow", function (user_id,id) {
    return Clinic.find({user_id:user_id,_id:id});
  });
}


/**      console.log(formType)
      console.log(result)
 * Create the schema for Stuff
 * See: https://github.com/aldeed/meteor-autoform#common-questions
 * See: https://github.com/aldeed/meteor-autoform#affieldinput
 */
 Clinic.attachSchema(new SimpleSchema({
  name: {
    label: "Name",
    type: String,
    optional: false,
    autoform: {
      placeholder: "Name"
    }
  },
  logo: {
    label: "logo",
    type: String,
    optional: false,
    autoform: {
      placeholder: "Logo"
    }
  },
  address: {
    label: "address",
    type: String,
    optional: false,
    autoform: {
      placeholder: "address"
    }
  },
  created_at : {
   type: Date,
   optional: true,
   autoform: {
     placeholder: ""
   }
  },
  user_id:{
   type: String,
   optional: true,
   autoform: {
     placeholder: ""
   }
  }
}));
