Template.ListPatient.helpers({

  /**
   * @returns {*} All of the Stuff documents.
   */
   patientList: function () {
    return Patient.find();
  }
});
